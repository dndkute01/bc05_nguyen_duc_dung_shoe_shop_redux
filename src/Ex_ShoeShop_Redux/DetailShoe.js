import React, { Component } from "react";
import { shoeReducer } from "./redux/reducer/shoeReducer";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    return (
      <div className="row pt-5 ">
        <img src={this.props.detail.image} className="col-4" alt="" />
        <div className="col-8 ">
          <p>{this.props.detail.name}</p>
          <p>{this.props.detail.price}</p>
          <p>{this.props.detail.description}</p>
          {/* detail */}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detail,
  };
};

export default connect(mapStateToProps)(DetailShoe);
