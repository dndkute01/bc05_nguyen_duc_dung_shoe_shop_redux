import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop_Redux extends Component {
  render() {
    return (
      <div className="container">
        <Cart />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}
// life cycle

let a = 2;
let b = a;
let c = b;
