import { dataShoe } from "./../../dataShoe";
import { ADD_TO_CART, CHANGE_DETAIL } from "./constant/shoeConstant";

let initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[1],
  cart: [],
};

export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        // th2: sp chưa có trong giỏ hàng
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        // th1 đã có
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case CHANGE_DETAIL: {
      state.detail = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
